# `cuda` Implementations:

This folder contains different color treatments to compute matrix-elements of gluon-scattering.

`/COLOR_SUMMED_F` Color-Summed matrix elements in the adjoiunt representation.

`/COLOR_SAMPLED_CF` Color-Sampled matrix elements, using the color-flow representation.

`/LEADING_COLOR` Color-Sampled matrix elements in the leading color approximation.

`/COLOR_DRESSED` Color-Sampled matrix elements using continuous color-dressing.

`/TESS` Original Tess code, implementing leading color apprximated matrix elements.

# Building

Move to one of the subfolders and execute:
```
mkdir build
cd build
cmake .. -DUSE_DOUBLE_PRECISION=1
make
```

Running the resulting executable without arguments will show the possible
parameters. In particular setting the number of block `-b NBLOCKS` and the
number of threads per block `-s NTHREADS` might be important depending on the
used hardware.
