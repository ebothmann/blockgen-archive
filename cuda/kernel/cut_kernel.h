// 
// COBG kernel
//
#include "config.h"


__global__ void cuts(bool *flags, const real *p, real *y, real *phi, const real pTCut, const real nuCut, const real dRCut){
  //__global__ void cuts(bool *flags, const real *p, const real pTCut, const real nuCut, const real dRCut){
  // check if the momenta satisfy the required cuts
  // here every outgoing particle is understood as a jet, thus no jed finding is involved
  // Global variables required
  //    - nin
  //    - nout
  //    - n
  //    - d_nev
  
  // thread-id
  int tid = (threadIdx.x + blockDim.x*blockIdx.x);
  int valid = 1;

  // check pseuderapidity  
  real pabs,nu ;
  for(int i = nin; i < n; i++){
    pabs = sqrt(pow(p[tid + (i*4+1)*d_nev],2)+pow(p[tid + (i*4+2)*d_nev],2)
		+pow(p[tid + (i*4+3)*d_nev],2));
    nu = 1./2 * log((pabs + p[tid + (i*4+3)*d_nev])/(pabs - p[tid + (i*4+3)*d_nev]));
    
    if (abs(nu) > nuCut){
      valid = 0;
    }
    __syncthreads();
    //valid *= (nu < nuCut);
  }

  real pt;
  for(int i = nin; i < n; i++){
    pt = sqrt(pow(p[tid + (i*4+1)*d_nev],2) + pow(p[tid + (i*4+2)*d_nev],2));
    
    //check transverse momentum    
    if (pt < pTCut){
      valid = 0;
    }
    __syncthreads();
    

    //valid *= (pt > pTCut);
    y[tid + i*d_nev] = 1./2. * log((p[tid + (i*4)*d_nev] + p[tid + (i*4+3)*d_nev]) /
		       (p[tid + (i*4)*d_nev] - p[tid + (i*4+3)*d_nev]));
    phi[tid + i*d_nev] = atan2(p[tid + (i*4+2)*d_nev], p[tid + (i*4+1)*d_nev]);
    if (phi[tid + i*d_nev] < 0.0) {phi[tid + i*d_nev] += 2*M_PI;}
    if (phi[tid + i*d_nev] >= 2*M_PI) {phi[tid + i*d_nev] -= 2*M_PI;}

    __syncthreads();
  }
  for(int i = nin; i < n; i++){
    for(int j = i+1; j < n; j++){
      if (sqrt(pow(y[tid + i*d_nev] - y[tid + j*d_nev],2)
	       + pow(phi[tid + i*d_nev] - phi[tid + j*d_nev],2)) < dRCut){
	valid = 0;
      }
      __syncthreads();
    }
  }

  // set the final flag
  flags[tid] = valid;
}

__global__ void cuts_verbose(bool *flags, const real *p, real *y, real *phi, const real pTCut, const real nuCut, const real dRCut, const int tid){
  //__global__ void cuts(bool *flags, const real *p, const real pTCut, const real nuCut, const real dRCut){
  // check if the momenta satisfy the required cuts
  // here every outgoing particle is understood as a jet, thus no jed finding is involved
  // Global variables required
  //    - nin
  //    - nout
  //    - n
  //    - d_nev
  
  // thread-id
  int valid = 1;

  // check pseuderapidity  
  real pabs,nu ;
  for(int i = nin; i < n; i++){
    pabs = sqrt(pow(p[tid + (i*4+1)*d_nev],2)+pow(p[tid + (i*4+2)*d_nev],2)
		+pow(p[tid + (i*4+3)*d_nev],2));
    nu = 1./2 * log((pabs + p[tid + (i*4+3)*d_nev])/(pabs - p[tid + (i*4+3)*d_nev]));

    
    if (abs(nu) > nuCut){
      valid = 0;
    }
    __syncthreads();
    printf("ncut[%d] = %d\n",i, valid);
    printf("pabs[%d] = %f\n",i,pabs);
    printf("nu[%d]   = %f\n",i,nu);
    //valid *= (nu < nuCut);
  }

  real pt;
  for(int i = nin; i < n; i++){
    pt = sqrt(pow(p[tid + (i*4+1)*d_nev],2) + pow(p[tid + (i*4+2)*d_nev],2));
    
    //check transverse momentum
    
    if (pt < pTCut){
      valid = 0;
    }
    __syncthreads();
    

    //valid *= (pt > pTCut);
    y[tid + i*d_nev] = 1./2. * log((p[tid + (i*4)*d_nev] + p[tid + (i*4+3)*d_nev]) /
		       (p[tid + (i*4)*d_nev] - p[tid + (i*4+3)*d_nev]));
    //phi[tid + i*d_nev] = asin(p[tid + (i*4+2)*d_nev] / pt);
    phi[tid + i*d_nev] = atan2(p[tid + (i*4+2)*d_nev], p[tid + (i*4+1)*d_nev]);
    if (phi[tid + i*d_nev] < 0.0) {phi[tid + i*d_nev] += 2*M_PI;}
    if (phi[tid + i*d_nev] >= 2*M_PI) {phi[tid + i*d_nev] -= 2*M_PI;}
    printf("y[%d]    = %f\n",i,y[tid+i*d_nev]);
    printf("phi[%d]  = %f\n",i,phi[tid + i*d_nev]);
    __syncthreads();
  }
  for(int i = nin; i < n; i++){
    for(int j = i+1; j < n; j++){
      if (sqrt(pow(y[tid + i*d_nev] - y[tid + j*d_nev],2)
	       + pow(phi[tid + i*d_nev] - phi[tid + j*d_nev],2)) < dRCut){
	valid = 0;
      }
      __syncthreads();
      
      //valid *= (sqrt(pow(y[tid + i*d_nev] - y[tid + j*d_nev],2)
      //	     + pow(phi[tid + i*d_nev] - phi[tid + j*d_nev],2)) > dRCut);
    }
  }

  printf("final flag = %d", valid);
  
  // set the final flag
  flags[tid] = valid;
}
