#include "config.h"

bool cuts(const int n, const real *p, const real pTCut, const real nuCut, const real dRCut){
  int valid = 1;
  
  // check pseuderapidity
  real pabs,nu ;
  for(int i = 2; i < n; i++){
    pabs = sqrt(pow(p[i*4+1],2)+pow(p[i*4+2],2)+pow(p[i*4+3],2));
    nu = 1./2 * log((pabs + p[i*4+3])/(pabs - p[i*4+3]));
    valid *= (abs(nu) < nuCut);
    //printf("pabs[%d] = %f\n",i,pabs);
    //printf("nu[%d]   = %f\n",i,nu);
  }
  
  // check distance
  real y[n];
  real phi[n];
  real pt;


  for(int i = 2; i < n; i++){
    pt = sqrt(pow(p[i*4+1],2) + pow(p[i*4+2],2));
    //check transverse momentum
    valid *= (pt > pTCut);
    y[i] = 1./2. * log((p[i*4] + p[i*4+3]) / (p[i*4] - p[i*4+3]));
    phi[i] = atan2(p[i*4+2],p[i*4+1]);
    if (phi[i] < 0.0) {phi[i] += 2*M_PI;}
    if (phi[i] >= 2*M_PI) {phi[i] -= 2*M_PI;}
    //printf("y[%d] = %f\n",i,y[i]);
    //printf("phi[%d] = %f\n",i,phi[i]);
    //asin(p[i*4+2] / pt);
  }
  for(int i = 2; i < n; i++){
    for(int j = i+1; j < n; j++){
      valid *= (sqrt(pow(y[i] - y[j],2) + pow(phi[i] - phi[j],2)) > dRCut);
    }
  }

  return (bool) valid;
}

bool cuts_verbose(const int n, const real *p, const real pTCut, const real nuCut, const real dRCut){
  int valid = 1;
  
  // check pseuderapidity
  real pabs,nu ;
  for(int i = 2; i < n; i++){
    pabs = sqrt(pow(p[i*4+1],2)+pow(p[i*4+2],2)+pow(p[i*4+3],2));
    nu = 1./2 * log((pabs + p[i*4+3])/(pabs - p[i*4+3]));
    valid *= (abs(nu) < nuCut);
    printf("ncut[%d] = %d\n",i, valid);
    printf("pabs[%d] = %f\n",i,pabs);
    printf("nu[%d]   = %f\n",i,nu);
  }
  
  // check distance
  real y[n];
  real phi[n];
  real pt;


  for(int i = 2; i < n; i++){
    pt = sqrt(pow(p[i*4+1],2) + pow(p[i*4+2],2));
    //check transverse momentum
    valid *= (pt > pTCut);
    y[i] = 1./2. * log((p[i*4] + p[i*4+3]) / (p[i*4] - p[i*4+3]));
    phi[i] = atan2(p[i*4+2],p[i*4+1]);
    if (phi[i] < 0.0) {phi[i] += 2*M_PI;}
    if (phi[i] >= 2*M_PI) {phi[i] -= 2*M_PI;}
    printf("y[%d]    = %f\n",i,y[i]);
    printf("phi[%d]  = %f\n",i,phi[i]);
    //asin(p[i*4+2] / pt);
  }
  for(int i = 2; i < n; i++){
    for(int j = i+1; j < n; j++){
      valid *= (sqrt(pow(y[i] - y[j],2) + pow(phi[i] - phi[j],2)) > dRCut);
    }
  }
  printf("final flag = %d", valid);
  return (bool) valid;
}

