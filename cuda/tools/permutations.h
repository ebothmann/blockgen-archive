#pragma once

#include "config.h"
#include "tools.h"
#include <vector>

class Permutations{
 public:
  int h_n;
  int n_perms;
  int n_fixed;
  int offs;
  int *perms;
  Permutations(const int _n, const int _n_fixed);
  ~Permutations();
  void createPerms();
  void createPermsLexi();
};

Permutations::Permutations(const int _n, const int _n_fixed){
  if(_n_fixed < 1){
    std::cerr << "n_fixed should be larger than 0, since the first index is always fixed" << std::endl;
  }
  n_fixed = _n_fixed;
  h_n = _n;
  n_perms = fact(h_n - n_fixed);
  std::cout << "n perms = " << n_perms << std::endl;
  offs = n_perms;
  perms = (int *)malloc(sizeof(int)*n_perms * h_n);
}

Permutations::~Permutations(){
  free(perms);
}

// Modified Version of Heap*s algorithm
//
// Suitable for the F-Basis computations, i.e. containing all permutations of
// (2,...,n-1) with 1, n fixed.
//
// https://en.wikipedia.org/wiki/Heap's_algorithm
void Permutations::createPerms(){
  int counter = 0;
  
  std::vector<int> A;
  int c[h_n];
  for(int i = 0; i<h_n; i++){
    A.push_back(i);
    c[i] = 1;
  }
  for(int j = 0; j<h_n; j++){
    perms[counter*h_n+j] = A[j];
  }
  ++counter;
  
  int i = 1;
  while(i<h_n - (n_fixed - 1)){
    if(c[i] < i){
      if(i % 2 == 1){
	int tmp = A[1];
	A[1] = A[i];
	A[i] = tmp;
      }
      else{
	int tmp = A[c[i]];
	A[c[i]] = A[i];
	A[i] = tmp;
      }
      for(int j = 0; j<h_n; j++){
	perms[counter*h_n+j] = A[j];
      }
      ++counter;
      ++c[i];
      i = 1;
    }
    else{
      c[i] = 1;
      ++i;
    }
  }
}


void Permutations::createPermsLexi(){
  int counter = 0;
  
  std::vector<int> A;
  for(int i = 0; i<h_n; i++){
    A.push_back(i);
  }
  for(int j = 0; j<h_n; j++){
    perms[counter*h_n+j] = A[j];
  }
  ++counter;
  
  while (true){
    int index = 0;
    for(int i = 0; i<h_n-2; i++){
      if(A[i]<A[i+1]) {	index = i; }
    }
    if (index == 0){ break; }

    int index2 = 0;
    for(int i = index+1; i<h_n-1; i++){
      if (A[index] < A[i]){ index2 = i; }
    }

    int tmp   = A[index];
    A[index]  = A[index2];
    A[index2] = tmp;

    for(int i = 0; i<(h_n-1-(index+1))/2; i++){
      int tmp2 = A[index+1+i];
      A[index+1+i] = A[h_n-2-i];
      A[h_n-2-i] = tmp2;
    }
    for(int j = 0; j<h_n; j++){
      perms[counter*h_n+j] = A[j];
    }
    ++counter;
  }
}

