#pragma once

#include "thrust/complex.h"

#include "BlockGen/Vector.cuh"
#include "BlockGen/Spinor.cuh"
#include "BlockGen/Event.cuh"

__device__
inline unsigned int NextPermutation(unsigned int inp) {
    unsigned int t = inp | (inp - 1);
    return (t + 1) | (((~t & -~t) - 1) >> (__clz(__brev(inp)) + 1));
}

__device__
inline void SetBits(unsigned int inp, unsigned int *set, unsigned int size) {
    unsigned int iset = 0;
    for(unsigned int i = 0; i < size; ++i) {
        if(inp & (1 << i)) set[iset++] = inp & (1 << i);
    }
}

namespace BlockGen {

class ColorDressed {
    using Complex = thrust::complex<double>;

    public:
        ColorDressed(const size_t &n) : m_n{n} {}
        ColorDressed(const ColorDressed &other) : m_n{other.m_n} {}

        __device__
        double CalcJL(Event&, const int) const;

        __host__ __device__
        size_t size() const { return m_n; } 

    private:
        __device__
        void GenerateCurrents(unsigned int, Event&, const int) const;

        __device__
        void SubCurrent(unsigned int, unsigned int, unsigned int,
                        unsigned int*, Event&, const int) const;

        __device__
        void Vertex3(unsigned int, unsigned int, unsigned int, Event&, const int) const;

        __device__
        Vec4D V3L(const Vec4D&, const Vec4D&, const Vec4D&, const Vec4D&) const;

        __device__
	Vec4D V3L_combined(const Vec4D&, const Vec4D&, const Vec4D&,
                           const Vec4D&, const Vec4D&, const Vec4D&) const;

        size_t m_n;
};

}

__global__
void EvaluateCDAmplitude(BlockGen::ColorDressed*, double*, BlockGen::Vec4D*, BlockGen::Vec4D*,
                         BlockGen::Tensor*, double*, const int);
