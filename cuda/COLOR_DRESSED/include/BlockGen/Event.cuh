#pragma once

#include "thrust/complex.h"
#include "BlockGen/Vector.cuh"
#include "BlockGen/Tensor.cuh"

namespace BlockGen {

struct Event {
    Vec4D *mom;
    Vec4D *current;
    Tensor *tensor;
    int max_cur, nexternal;
};

}

__device__
inline unsigned int Cur(unsigned int ipart) {
    return (1 << ipart) - 1;
}

__device__
inline unsigned int CurIdx(unsigned int cur, unsigned int i, unsigned int j) {
    return 9*cur + 3*i + j;
}
