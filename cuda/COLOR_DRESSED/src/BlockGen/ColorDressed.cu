#include "BlockGen/ColorDressed.cuh"

#define MAX_PARTICLES 30

using namespace BlockGen;

__device__
double ColorDressed::CalcJL(Event &event, const int d_nev) const {
  for(size_t i = 2; i < m_n; ++i) GenerateCurrents(i, event, d_nev);
  
  double amp = 0;
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 3; ++j) {
      amp += event.current[CurIdx(event.max_cur-1,i,j)*d_nev]
	*event.current[CurIdx(event.max_cur-2,j,i)*d_nev];
    }
  } 
#ifdef CDDEBUG
  if(threadIdx.x==0 && blockIdx.x==0) {
      printf("amp = (%f, %f)\n", amp.real(), amp.imag());
      printf("amp2 = %f\n", thrust::norm(amp));
  }
#endif
  return amp*amp;
}

__device__
void ColorDressed::GenerateCurrents(unsigned int m, Event &e, const int d_nev) const {
  unsigned int cur = (1 << m) - 1;
  unsigned int set[MAX_PARTICLES];
  // Combine all currents but the last one
  while(cur < (1 << (m_n - 1))) {
    SetBits(cur, set, m_n-1);
    for(unsigned int iset = 1; iset < m; ++iset) {
      SubCurrent(cur, iset, m, set, e, d_nev);
    }
    
    cur = NextPermutation(cur);
  }
}

__device__
void ColorDressed::SubCurrent(unsigned int cur, unsigned int iset, unsigned int nset,
                        unsigned int *set, Event &event, const int d_nev) const {
  unsigned int idx = (1 << iset) - 1;
  while(idx < (1 << (nset - 1))) {
    unsigned int subCur1 = 0;
    for(unsigned int i = 0; i < m_n; ++i) {
      subCur1 += set[i]*((idx >> i) & 1);
    }
    auto subCur2 = cur ^ subCur1;
    Vertex3(cur, subCur1, subCur2, event, d_nev);
    idx = NextPermutation(idx);
  }
}

__device__
Vec4D ColorDressed::V3L(const Vec4D &p1, const Vec4D &p2,
                   const Vec4D &j1, const Vec4D &j2) const {  
  return invsqrttwo*(j1*j2)*(p1-p2) + sqrttwo*((j1*p2)*j2-(j2*p1)*j1);
}


__device__
Vec4D ColorDressed::V3L_combined(const Vec4D &p1, const Vec4D &p2,
			    const Vec4D &j11, const Vec4D &j12, const Vec4D &j21, const Vec4D &j22) const {
  // this should save at least two memory reads
  return invsqrttwo*((j11*j12)-(j21*j22))*(p1-p2) + sqrttwo*((j11*p2)*j12-(j21*p2)*j22-(j12*p1)*j11+(j22*p1)*j21);
}


__device__
void ColorDressed::Vertex3(unsigned int cur, unsigned int sub1, unsigned int sub2,
                     Event &e, const int d_nev) const {
  cur -= 1;
  sub1 -= 1;
  sub2 -= 1;
  
  double coupling = sqrt(0.118*4*M_PI);
  double coupling2 = sqrt(0.118*4*M_PI/2);
  e.mom[cur*d_nev] = e.mom[sub1*d_nev] + e.mom[sub2*d_nev];
  double denom = cur == (e.max_cur-2) ? 1 : e.mom[cur*d_nev].Abs2();
  
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 3; ++j) {
      for(int k = 0; k < 3; ++k) {
	// Vector <- Vector-Vector vertex
	auto tmp_current = V3L_combined(e.mom[sub1*d_nev], e.mom[sub2*d_nev],
					e.current[CurIdx(sub1, i, k)*d_nev], e.current[CurIdx(sub2, k, j)*d_nev],
					e.current[CurIdx(sub1, k, j)*d_nev], e.current[CurIdx(sub2, i, k)*d_nev]) * coupling;
	
	// Vector <- Tensor-Vector vertex
	tmp_current += (e.tensor[CurIdx(sub1, i, k)*d_nev]*e.current[CurIdx(sub2, k, j)*d_nev]
			-e.tensor[CurIdx(sub1, k, j)*d_nev]*e.current[CurIdx(sub2, i, k)*d_nev])* coupling2;
	tmp_current += (e.tensor[CurIdx(sub2, i, k)*d_nev]*e.current[CurIdx(sub1, k, j)*d_nev]
			-e.tensor[CurIdx(sub2, k, j)*d_nev]*e.current[CurIdx(sub1, i, k)*d_nev])* coupling2;
	
	e.current[CurIdx(cur, i, j)*d_nev] += tmp_current / denom;
	
	// Tensor <- Vector-Vector vertex
	e.tensor[CurIdx(cur, i, j)*d_nev] += (Tensor(e.current[CurIdx(sub1, i, k)*d_nev],
						     e.current[CurIdx(sub2, k, j)*d_nev])
					      - Tensor(e.current[CurIdx(sub1, k, j)*d_nev],
						       e.current[CurIdx(sub2, i, k)*d_nev])) * coupling2;
      }
    }
  }

#ifdef CDDEBUG
  for(int i = 0; i < 3; ++i) {
      for(int j = 0; j < 3; ++j) {
        if(threadIdx.x==0 && blockIdx.x==0) {
            auto current = e.current[CurIdx(cur, i, j)*d_nev];
            printf("current(%d) (%d, %d) = {(%f, %f), (%f, %f), (%f, %f), (%f, %f)}\n", cur, i, j,
                   current[0].real(), current[0].imag(),
                   current[1].real(), current[1].imag(),
                   current[2].real(), current[2].imag(),
                   current[3].real(), current[3].imag());
        }
      }
  }

   for(int i = 0; i < 3; ++i) {
        for(int j = 0; j < 3; ++j) {
          if(threadIdx.x==0 && blockIdx.x==0) {
              auto current = e.tensor[CurIdx(cur, i, j)*d_nev];
              printf("tensor(%d) (%d, %d) = {(%f, %f), (%f, %f), (%f, %f), (%f, %f), (%f, %f), (%f, %f)}\n",
                     cur, i, j,
                     current[0].real(), current[0].imag(),
                     current[1].real(), current[1].imag(),
                     current[2].real(), current[2].imag(),
                     current[3].real(), current[3].imag(),
                     current[4].real(), current[4].imag(),
                     current[5].real(), current[5].imag());
          }
        }
    }
#endif
}

__global__
void EvaluateCDAmplitude(ColorDressed *cd, double *amps2, Vec4D *mom,
                         Vec4D *cur, Tensor *tensor, double *wgts, const int d_nev) {
  // Load event
  auto idx = blockIdx.x*blockDim.x+threadIdx.x;
  Event event;
  event.max_cur = 1 << (cd->size() - 1); 
  event.mom     = &mom[idx];
  event.current = &cur[idx];
  event.tensor  = &tensor[idx];

  amps2[idx] = cd -> CalcJL(event, d_nev)*wgts[idx];
}
