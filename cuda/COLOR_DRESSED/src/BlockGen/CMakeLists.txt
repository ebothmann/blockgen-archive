add_executable(spinor main.cu ColorDressed.cu Rambo.cu)
set_target_properties(spinor PROPERTIES CUDA_SEPARABLE_COMPILATION ON)
target_link_libraries(spinor PRIVATE program_options PUBLIC fmt::fmt cudainterface)
install(TARGETS spinor cudainterface
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
    RUNTIME DESTINATION bin
    INCLUDES DESTINATION include
    )
