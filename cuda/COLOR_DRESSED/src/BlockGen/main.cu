#include <thrust/device_ptr.h>
#include <thrust/count.h>
#include <thrust/execution_policy.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/functional.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/random.h>
#include <thrust/execution_policy.h>

#include <chrono>
#include <cstdlib>
#include <iostream>
#include <random>

//__constant__ int d_nev;

#include "BlockGen/Rambo.cuh"
#include "BlockGen/ColorDressed.cuh"
#include "BlockGen/Statistics.cuh"
#include "cuda/Interface.cuh"

// #include "common/errors.h"

#include "CLI11/CLI11.hpp"

using namespace BlockGen;

constexpr size_t factorial(size_t n) {
    return n == 0 ? 1 : n * factorial(n-1);
}

struct is_nonzero {
    __host__ __device__
    bool operator()(const double &x) {
        return x != 0.0;
    }
};

int main(int argc, char** argv) {
    CLI::App app{"Calculation of gluon amplitudes using a GPU"};

    // Run parameters
    size_t nevents;// = 1 << 10;
    size_t nexternal = 4;
    double ecm = 14000;

    int nblocks;// = 80;
    int threads;// = 1024;
    size_t seed = 123456789;
    
    app.add_option("-e,--nevents", nevents, "The number of events");
    app.add_option("-g,--ngluons", nexternal, "The number of gluons");
    app.add_option("-s,--ecm",     ecm, "The center of mass energy");
    app.add_option("-b,--nblocks", nblocks, "The number of blocks");
    app.add_option("-t,--threads", threads, "The number of threads per block");
    app.add_option("--seed", seed, "The random seed");

    
    // Cuts
    double ptCut = 20;
    double etaCut = 2.5;
    double deltaRCut = 0.4;
    app.add_option("--ptCut", ptCut, "Minimum allowed pt");
    app.add_option("--etaCut", etaCut, "Maximum allowed eta");
    app.add_option("--deltaRCut", deltaRCut, "Minimum allowed DeltaR");


    CLI11_PARSE(app, argc, argv);
    nevents = nblocks * threads;
    
    std::cout << "Number of events: " << nevents << std::endl;
    auto devices = BlockGen::cuda::GetDevices();
    BlockGen::cuda::SetDevice(0);

    MemoryPair<ColorDressed> cd(1, nexternal);
    MemoryPair<Rambo> rambo(1, 2, nexternal-2, ptCut, etaCut, deltaRCut);

    MemoryPair<Vec4D> momentum((1 << nexternal)*nevents);
    Vec4D *devCur = AllocateOnGpu<Vec4D>((1 << nexternal)*nevents*9*sizeof(Vec4D));
    Tensor *devTen = AllocateOnGpu<Tensor>((1 << nexternal)*nevents*9*sizeof(Tensor));
    MemoryPair<double> weights(nevents);
    double *rans = AllocateOnGpu<double>((10*nexternal-8)*nevents*sizeof(double));

    MemoryPair<double> amps2(nevents);
    curandState *devStates = AllocateOnGpu<curandState>(nevents*sizeof(curandState));
    std::cout << nexternal << " " << sizeof(ColorDressed) + sizeof(Rambo) << " " << sizeof(Vec4D)*(1 << nexternal) + sizeof(double)*(+(9*nexternal-8)) + (1 << nexternal)*9*sizeof(Vec4D) + (1 << nexternal)*9*sizeof(Tensor) << " "<< 0 << std::endl;
    
    for(size_t i = 0; i < nevents; ++i) {
      momentum.host[i]         = Vec4D(ecm/2, 0, 0, ecm/2);
      momentum.host[i+nevents] = Vec4D(ecm/2, 0, 0, -ecm/2);
    }

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    momentum.ToGpu();
    rambo.ToGpu();
    cd.ToGpu();

    printf("nev = %d, blocks = %d, threads = %d, nev/32 = %d\n", nevents, nblocks, threads, nevents/32);

    std::vector<double> means, vars;
   
    float milliseconds = 0;
    cudaEventRecord(start);
    SeedRandom<<<nevents/32, 32>>>(devStates, seed);
    cudaDeviceSynchronize();
    CheckCudaCall(cudaPeekAtLastError());

    GenerateMomenta<<<nevents/32, 32>>>(devStates, rambo.device, momentum.device,
					devCur, weights.device, rans, nevents);
    cudaDeviceSynchronize();

    momentum.FromGpu();
    weights.FromGpu();

    CheckCudaCall(cudaPeekAtLastError());
    EvaluateCDAmplitude<<<nblocks, threads>>>(cd.device, amps2.device, momentum.device,
                                              devCur, devTen, weights.device, nevents);

    cudaDeviceSynchronize();
    CheckCudaCall(cudaGetLastError());
    cudaEventRecord(stop);

    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&milliseconds, start, stop);
    fmt::print("For {} gluons it took {:.3e} ms for {} events => {:.3e} ms/event\n",
               nexternal, milliseconds, nevents, milliseconds/(nevents));
    std::cout << "PROF: " << milliseconds/(nevents)/1000 << std::endl;

    // setup stats
    summary_stats_unary_op<double> unary_op;
    summary_stats_binary_op<double> binary_op;
    summary_stats_data<double> init;
    init.initialize();

    cudaEventRecord(start);
    auto result = thrust::transform_reduce(thrust::device, amps2.device, amps2.device + nevents,
                                           unary_op, init, binary_op);
    means.push_back(result.mean);
    vars.push_back(result.variance());
    cudaDeviceSynchronize();
    CheckCudaCall(cudaGetLastError());
    cudaEventRecord(stop);

    cudaEventSynchronize(stop);

    size_t free_byte ;
    size_t total_byte ;
    auto cuda_status = cudaMemGetInfo( &free_byte, &total_byte ) ;
    printf("free mem = %f %%\n", (float) free_byte / (1.*total_byte));    
    
    milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);
    constexpr double initialStateAvg = 16*16;
    constexpr double hbarc2 = 3.89379304e8;
    double nfactorial = factorial(nexternal-2);
    double flux = 2*ecm*ecm;
    fmt::print("sym = {}\n", initialStateAvg*nfactorial);
    fmt::print("flux = {}\n", 1/flux);
    double prefactors = hbarc2/flux/initialStateAvg/nfactorial;
    double xsec = result.mean*prefactors;
    double xsec_err = sqrt(result.variance()/nevents)*prefactors;
    fmt::print("Reduction took {:.3e} ms for {} events with a result of {:.3e} +/- {:.3e} pb\n",
               milliseconds, nevents, xsec, xsec_err);
    double cut_eff = thrust::count_if(thrust::device, amps2.device, amps2.device + nevents,
                                      is_nonzero());
    fmt::print("Cut efficiency: {:.3f}\n", cut_eff/nevents);
    fmt::print("Max weight: {:.3e}\n", result.max*prefactors);
    fmt::print("{} {}\n", xsec, xsec_err);
}
