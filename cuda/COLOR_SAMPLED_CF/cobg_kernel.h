//
// COBG kernel
//

#include "config.h"
#include "../../tools/vec4.h"

__device__ inline int index(const int k, const int l){
  int m = l-k+1;
  return ((m-1)*(n-m/2.) + k);
}

__device__ vec4 V3Init(const vec4 Ja, const vec4 Jb, const vec4 Pa, const vec4 Pb,
		       vec4 *P, const int ind){
  P[ind] = Pa + Pb;
  return  2*(Ja*Pb)*Jb - 2*(Pa*Jb)*Ja + (Ja*Jb)*(Pa-Pb);
}

__device__ vec4 V3(const vec4 Ja, const vec4 Jb, const vec4 Pa, const vec4 Pb){
  return  2*(Ja*Pb)*Jb - 2*(Pa*Jb)*Ja + (Ja*Jb)*(Pa-Pb);
}

__device__ vec4 V4(const vec4 Ja, const vec4 Jb, const vec4 Jc){
  return 2*(Ja*Jc)*Jb - (Ja*Jb)*Jc - (Jc*Jb)*Ja;
}

__global__ void cobg(vec4 *J, vec4 *PP, const real *p, const real *rans, real *A,
		     int *nperms, int *valids, int *perms, int nthreads, int *indices,
		     real *pol_vectors){

  const int tidx = (threadIdx.x + blockDim.x*blockIdx.x);
  const int tid = indices[tidx];
    
  vec4 current;
  int perm;
  int pol_index;

  // set constant momenta and currents
  extern __shared__ vec4 PPP[];
  vec4 *P = &PPP[threadIdx.x * (n+1)];
  
  if(tidx < nthreads){
    int n_perms = nperms[nthreads - tidx - 1];

    for(int j=0; j<4; j++){
      P[0].data[j] = 0;
    }
    for( int j = 0; j<4; j++){
      J[tid + ((NN + 0)*d_nev)][j] = pol_vectors[tid + ((n-1)*8 + 2*j+0)*d_nev];
      //J[tid + ((NN + 1)*d_nev)][j] = pol_vectors[tid + ((n-1)*8 + 2*j+1)*d_nev];
    }
    
    for(int jj = 0; jj < n_perms; ++jj){
      int i_perm = valids[tid + jj*d_nev];

      for(int i=0; i<n; i++){
	for(int j=0; j<4; j++){
	  P[i+1].data[j] = P[i][j] + p[tid + (perms[n * i_perm + i]*4+j)*d_nev];
	}
      }

      
      for(int i=0; i<n-1; i++){
	//perm = shuffle[n*i_perm + i];
	perm = perms[n*i_perm + i];
	pol_index  = tid;          // move to right column
	pol_index += perm*8*d_nev; // move to right start of pol vectors
	//pol_index += pols[tid + perm*d_nev]*d_nev; // choose right polarisation
	
	for( int j = 0; j<4; j++){
	  J[tid + (i*d_nev)][j] = pol_vectors[pol_index + 2*j*d_nev];
	}
      }    

      
      for(int m=1; m<n-2; m++){
	for(int k=0; k<n-m-1; k++){
	  current = V3(J[tid+index(k,k)*d_nev],
		       J[tid+index(k+1,k+m)*d_nev],
		       P[k+1] - P[k],
		       P[k+m+1] - P[k+1]);
	  
	  for(int i=k+1; i<k+m; i++){
	    current += V3(J[tid+index(k,i)*d_nev],
			  J[tid+index(i+1,k+m)*d_nev],
			  P[i+1] - P[k],
			  P[k+m+1] - P[i+1]);
	  }
	  for(int i=k; i<k+m; i++){
	    for(int j = i+1; j<k+m;j++){
	      current += V4(J[tid + index(k,i)*d_nev],
			    J[tid + index(i+1,j)*d_nev],
			    J[tid + index(j+1,k+m)*d_nev]);
	    }	
	  }
	  J[tid + index(k,k+m)*d_nev] = current/((P[k+m+1] - P[k])*(P[k+m+1]-P[k]));
	}
      }
      for(int m=n-2; m<n; m++){
	for(int k=0; k<n-m-1; k++){
	  current = V3(J[tid+index(k,k)*d_nev],
		       J[tid+index(k+1,k+m)*d_nev],
		       P[k+1] - P[k],
		     P[k+m+1] - P[k+1]);
	  for(int i=k+1; i<k+m; i++){
	    current += V3(J[tid+index(k,i)*d_nev],
			  J[tid+index(i+1,k+m)*d_nev],
			  P[i+1]-P[k],
			P[k+m+1]-P[i+1]);
	  }
	  for(int i=k; i<k+m; i++){
	    for(int j = i+1; j<k+m;j++){
	      current += V4(J[tid + index(k,i)*d_nev],
			    J[tid + index(i+1,j)*d_nev],
			    J[tid + index(j+1,k+m)*d_nev]);
	    }	
	  }
	  J[tid + index(k,k+m)*d_nev] = current;
	}
      }
      A[tid] += J[tid + NN*d_nev]*J[tid + (NN-1)*d_nev];
    }
  }
}
