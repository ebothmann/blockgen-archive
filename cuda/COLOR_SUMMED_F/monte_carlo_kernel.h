#include "config.h"

__global__ void MonteCarloColorSummed(real *ret_mc,
				      const real *xf1,
				      const real *xf2,
				      const real *x1,
				      const real *x2,
				      const real *me,
				      const real *psweight,
				      const real *alphas, 
				      const bool *cuts,
				      const int symmetry_factor){
  const int tid = threadIdx.x + blockDim.x*blockIdx.x;
  const real conversion = 0.389379*1e9;

  real dcs = 1.0;
  dcs *= xf1[tid] * xf2[tid] / (pow(x1[tid],2) * pow(x2[tid],2));
  dcs /= 2 * ecm*ecm;
  dcs *= me[tid];
  dcs /= symmetry_factor;
  dcs *= pow(4 * M_PI *alphas[tid] ,n-2);
  dcs *= psweight[tid];
  dcs *= conversion;
  dcs /= 64.;
  dcs *= (int) cuts[tid];
  ret_mc[tid] = dcs;
}


