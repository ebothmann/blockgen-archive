# project settings
cmake_minimum_required(VERSION 2.8)
project(cuco)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

# optionally enable CUDA support
find_package(CUDA)

# find external libs
find_package(PkgConfig REQUIRED)
pkg_check_modules(LHAPDF lhapdf>=6.2.1)

# set floating point precision
if( USE_DOUBLE_PRECISION )
  message("Nout set manually to ${USE_DOUBLE_PRECISION}")
else()
  set(USE_DOUBLE_PRECISION)
  message("Nout set automatically to ${USE_DOUBLE_PRECISION}")
endif()



# generate config file
configure_file (
  "${PROJECT_SOURCE_DIR}/config.h.in"
  "${PROJECT_BINARY_DIR}/config.h"
  )
include_directories("${PROJECT_BINARY_DIR}")

if(LHAPDF_FOUND)
  link_directories(${LHAPDF_LIBDIR})
endif()


if(CUDA_FOUND)
  add_library(project_cuda_flags INTERFACE)
  include(CMake/CudaFlags.cmake)
  set(CUDA_PROPAGATE_HOST_FLAGS OFF)
  cuda_add_executable(cobg_gpu cobg.cu)
  target_link_libraries(cobg_gpu ${CUDA_curand_LIBRARY})
endif()
