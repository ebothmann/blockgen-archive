#ifndef SYSTEMH
#define SYSTEMH

#include "config.h"

std::string get_environment_variable(const std::string& key, const std::string& def=""){
  char* val = std::getenv(key.c_str());
  return (val == NULL) ? def : std::string(val);
}

#endif
