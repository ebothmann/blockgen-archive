//
// Defining pdf by linear interpolation from data file containing
// precomputed alphas values
//
// Assumes the data file is organised, such that the first two lines contain
// info about minimal and maximal values of mu, as well as the steps
// inbetween the two.
//
// Furthermore the data should be distributed logarithmically
//


#ifndef INTERPOLPDFH
#define INTERPOLPDFH

#include <iostream>
#include <fstream>
#include <math.h>
#include <vector>
#include "config.h"

class InterpolPdf{
 public:
  real_float info[6];
  std::vector<real_float> als;
  
  InterpolPdf(const std::string filename);
  real_float index(const int i, const int j);
  real_float getPdf(const real_float x, const real_float q2);
  real_float getPdfWiki(const real_float x, const real_float q2);
  void printInfo();
};

InterpolPdf::InterpolPdf(const std::string filename){
  // read in file
  std::string line;
  std::ifstream datafile (filename);

  // helper indices
  int ind = 0;
  int count = 0;
  if (datafile.is_open()){
    // read first line, containing setup infos of the qsteps
    getline (datafile,line);
    for (size_t i = 0; i<line.length(); i++){
      if (line[i] == ' '){
	info[count] = stod(line.substr(ind,i-ind));
	count++;
	ind = i+1;
      }
    }
    // read second line, containing setup infos of the xsteps
    getline (datafile,line);
    ind = 0;
    for (size_t i = 0; i<line.length(); i++){
      if (line[i] == ' '){
	info[count] = stod(line.substr(ind,i-ind));
	count++;
	ind = i+1;
      }
    }

    count = 0;
    ind = 0;
    for(size_t j = 0; j<info[2]+1; j++){
      getline (datafile,line);
      ind = 0;
      count = 0;	
      for (size_t i = 0; i<line.length(); i++){
	if (line[i] == ' '){
	  als.push_back(stod(line.substr(ind,i-ind)));
	  count++;
	  ind = i+1;
	}
      }
    }
    datafile.close();
  }
  else{
    std::cerr << "Could not find file" << std::endl;
  }
}

real_float InterpolPdf::index(const int i, const int j){
  // i -> x, j -> q2
  // access the vector in the right position
  return als[(info[2]+1) * (int) j + (int) i];
}

real_float InterpolPdf::getPdfWiki(const real_float x, const real_float q2){
  const real_float lq2 = log(q2);
  const real_float lx  = log(x);

  const real_float lq2min = log(info[0]);
  const real_float lq2max = log(info[1]);  
  const real_float lxmin = log(info[3]);
  const real_float lxmax = log(info[4]);
  const real_float lq2range = lq2max - lq2min;
  const real_float lxrange = lxmax - lxmin;
  const real_float q2bin = (lq2 - lq2min) / (lq2range / info[2]);
  const real_float xbin = (lx - lxmin) / (lxrange / info[5]);

  const real_float q2remainder = q2bin - (int) q2bin;
  const real_float xremainder = xbin - (int) xbin;
    
  return getPdf((int) q2bin, (int) xbin) * (1-q2remainder) * (1-xremainder)
    + getPdf((int) q2bin + 1, (int) xbin) * q2remainder * (1-xremainder)
    + getPdf((int) q2bin, (int) xbin + 1) * (1-q2remainder) * xremainder
    + getPdf((int) q2bin + 1, (int) xbin + 1) * q2remainder * xremainder;
}

real_float InterpolPdf::getPdf(const real_float x, const real_float q2){
  const real_float lq2 = log(q2);
  const real_float lx  = log(x);

  const real_float lq2min = log(info[0]);
  const real_float lq2max = log(info[1]);  
  const real_float lxmin = log(info[3]);
  const real_float lxmax = log(info[4]);
  const real_float lq2range = lq2max - lq2min;
  const real_float lxrange = lxmax - lxmin;
  const real_float q2bin = (lq2 - lq2min) / (lq2range / info[2]);
  const real_float xbin = (lx - lxmin) / (lxrange / info[5]);

  const real_float q2remainder = q2bin - (int) q2bin;
  const real_float xremainder = xbin - (int) xbin;


  
  // Finally the Interpol
  const real_float q0 = index(xbin,q2bin)
    + xremainder*(index(xbin+1, q2bin) - index(xbin, q2bin));
  const real_float q1 = index(xbin, q2bin+1)
    + xremainder*(index(xbin+1, q2bin+1) - index(xbin, q2bin+1));
  
  return q0 + q2remainder * (q1 - q0);
}
void InterpolPdf::printInfo(){
  std::cout << "q2: " << info[0] << " " << info[1] << " " << info[2] << std::endl;
  std::cout << "x : " << info[3] << " " << info[4] << " " << info[5] << std::endl;
}



#endif
