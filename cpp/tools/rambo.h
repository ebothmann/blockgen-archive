#include <iostream>
#include <math.h>
#include <cstdlib>
#include "config.h"
#include "tools.h"

class Rambo{
  real_float ecm;
  real_float GammaProd;
  int nin;
  int nout;
    
public:
  Rambo(int _nin, int _nout, real_float ecms);
  void generatePoint(real_float *p, const real_float x1, const real_float x2);
  real_float generateWeight(real_float *p, real_float x1, real_float x2);
};

inline real_float getRand(){
  return std::rand()*1./RAND_MAX;
}

Rambo::Rambo(int _nin, int _nout, real_float ecms){
  nin = _nin;
  nout = _nout;
  ecm = ecms;
  GammaProd = fact(nout-1)*fact(nout-2);
}

void Rambo::generatePoint(real_float *p, const real_float x1, const real_float x2){
  // Incoming particles
  p[0] = -x1 * ecm/2.;
  p[1] = 0;
  p[2] = 0;
  p[3] = -x1 * ecm/2.;
  p[4] = -x2 * ecm/2.;
  p[5] = 0;
  p[6] = 0;
  p[7] = x2 * ecm/2.;



  // in cms frame, p1+p2 = (w,0,0,0)
  real_float w = sqrt(x1*x2)*ecm;

  // Explicit boost in cms frame to check if w is computed correctly
  /*
  real_float beta = (x1-x2)/(x1+x2);
  real_float gamma = sqrt(1. / (1.-beta*beta));
  real_float e,pz;
  for(int i = 0; i<nin; i++){
    e = moms[4*i+0];
    pz = moms[4*i+3];
    p[4*i+0] = gamma*(e-beta*pz);
    p[4*i+3] = gamma*(pz-beta*e);
  }
  real_float p_in[4] = {0,0,0,0};
  for(int i = 0; i<nin; i++){
    for(int j = 0; j<4; j++){
      p_in[j] += p[4*i+j];
    }
  }
  real_float w_check = p_in[0];
  */
  
  
  // Apply trafo (3.1) in Computer Physics Communications 40 (1986) 359—373
  real_float c, phi;
  for(int i = nin; i<nin + nout; i++){
    c = 2*getRand() -1 ;
    phi = 2*M_PI*getRand();
    p[4*i + 0] = -log(getRand()*getRand());
    p[4*i + 1] = p[4*i + 0] * sqrt(1-c*c) * cos(phi);
    p[4*i + 2] = p[4*i + 0] * sqrt(1-c*c) * sin(phi);
    p[4*i + 3] = p[4*i + 0] * c;
  }
  
  // Apply trafo (2.5) in Computer Physics Communications 40 (1986) 359—373
  real_float Q[4] = {0,0,0,0};
  for (int i=nin; i<nin+nout; i++){
    for (int j=0; j<4; j++){
      Q[j] += p[4*i+j];
    }
  }
  
  real_float M = sqrt(Q[0]*Q[0] - Q[1]*Q[1] - Q[2]*Q[2] - Q[3]*Q[3]);
  real_float gamma = Q[0] / M;
  real_float a = 1./(1+gamma);
  real_float x = w/M;
  real_float b[4] = {-Q[0]/M, -Q[1]/M, -Q[2]/M, -Q[3]/M};

  // Apply trafo (2.4) in Computer Physics Communications 40 (1986) 359—373
  real_float bq, q0;
  for(int i = nin; i<nin+nout; i++){
    bq = b[1]*p[4*i + 1] + b[2]*p[4*i + 2] + b[3]*p[4*i + 3];
    q0 = p[4*i + 0];
    p[4*i + 0] = x * (gamma * q0 + bq);
    p[4*i + 1] = x * (p[4*i + 1] + b[1]*q0 + a*bq*b[1]);
    p[4*i + 2] = x * (p[4*i + 2] + b[2]*q0 + a*bq*b[2]);
    p[4*i + 3] = x * (p[4*i + 3] + b[3]*q0 + a*bq*b[3]);
  }
  // boost the momenta in the respective rest frame
  // Lorentzboost
  real_float beta = -(x1-x2)/(x1+x2);
  gamma = sqrt(1. / (1.-beta*beta));
  real_float e,pz;
  for(int i = nin; i<nout+nin; i++){
    e = p[4*i+0];
    pz = p[4*i+3];
    p[4*i+0] = gamma*(e-beta*pz);
    p[4*i+3] = gamma*(pz-beta*e);
  }
}

real_float Rambo::generateWeight(real_float *p, real_float x1, real_float x2){
  // Corresponds to (2.14) in Computer Physics Communications 40 (1986) 359—373
  const real_float w = sqrt(x1*x2)*ecm;
  const real_float dlips = pow(2*M_PI,4) *1./ pow(2*M_PI,3*nout);
  //const real_float dlips = 1.;
  const real_float Vn = pow(M_PI / 2., nout-1) * pow(w, 2*nout-4) / GammaProd * dlips;
  return Vn;
}
