//
// Defining alphas by linear interpolation from data file containing
// precomputed alphas values
//
// Assumes the data file is organised, such that the first line contains
// info about minimal and maximal values of mu, as well as the steps
// inbetween the two.
//
// Furthermore the data should be distributed logarithmically
//


#ifndef ALPHASH
#define ALPHASH

#include <iostream>
#include <fstream>
#include <math.h>
#include <vector>
#include "config.h"

inline real_float logb(const real_float base, const real_float x){
  return log(x) / log(base);
}

class Alphas{
 public:
  real_float info[3];
  std::vector<real_float> als;

  Alphas(const std::string filename);
  real_float getAlphas(const real_float mu);
  real_float getAlphas(const int index);
  real_float getAlphasLinear(const real_float mu);
  void printInfo();
};

Alphas::Alphas(const std::string filename){
  // read in file
  std::string line;
  std::ifstream datafile (filename);

  // helper indices
  int ind = 0;
  int count = 0;
  if (datafile.is_open()){
    // read first line, containing setup infos
    getline (datafile,line);
    for (unsigned int i = 0; i<line.length(); i++){
      if (line[i] == ' '){
	info[count] = stod(line.substr(ind,i-ind));
	count++;
	ind = i+1;
      }
    }
    count = 0;
    ind = 0;
    getline (datafile,line);
    for (unsigned int i = 0; i<line.length(); i++){
      if (line[i] == ' '){
	als.push_back(stod(line.substr(ind,i-ind)));
	count++;
	ind = i+1;
      }
    }
    datafile.close();
  }
  else{
    std::cerr << "Could not find file" << std::endl;
  }
}
real_float Alphas::getAlphas(const real_float mu){
  if(mu < info[0] || info[1] < mu){
    std::cerr << "Requested alphas outside of range!\nRequested: mu = " << mu << ", Range = ["
	      << info[0] << ", " << info[1] << "]\n";
    return -1;
  }
  // find out which bin mu is in
  const real_float xrange = info[1] / info[0];
  const real_float fact  = pow(xrange,1./info[2]);
  const real_float bin = logb(fact, mu/info[0]);
  const real_float remainder = mu / (info[0] * pow(fact,(int) bin));
  return (1 - remainder / fact ) *  als[(int) bin] + remainder / fact * als[(int) bin+1];
}

real_float Alphas::getAlphasLinear(const real_float mu){
  if(mu < info[0] || info[1] < mu){
    std::cerr << "Requested alphas outside of range!\nRequested: mu = " << mu << ", Range = ["
	      << info[0] << ", " << info[1] << "]\n";
    return -1;
  }
  // find out which bin mu is in
  const real_float lmu = log(mu);
  const real_float lxmin = log(info[0]);
  const real_float lxmax = log(info[1]);
  const real_float lrange = lxmax - lxmin;
  const real_float bin = (lmu - lxmin) / (lrange / info[2]);
  const real_float remainder = bin - (int) bin;
  return als[(int) bin] + remainder * (als[(int) bin +1] - als[(int) bin]);
}


// not real_floatly usefull, but good for testing purposes
real_float Alphas::getAlphas(const int index){
  return als[index];
}

void Alphas::printInfo(){
  std::cout << "xmin = " << info[0]
	    << ", xmax = " << info[1]
	    << ", N = " << info[2] << std::endl;

}
#endif
