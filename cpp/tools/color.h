#include "config.h"
#include "tools.h"
#include <vector>
#include <cstring>

class ColorIntegrator{
  int n;
  int NC;
  bool found = false;
  
 public:
  int *shuf;
  int *colors;
  
  std::vector<std::vector<int>> perms;
  ColorIntegrator(const int _n, const int _NC);
  ~ColorIntegrator();
  void UniformColors();
  void Colors();
  void NewColors();
  bool trivialcheck();
  bool findperms();
  bool findperms(std::vector<int> perm, std::vector<int> inds);
  real_float UniformWeight();
  real_float Weight();
  real_float NewWeight();
};
  
ColorIntegrator::ColorIntegrator(const int _n, const int _NC){
  n  = _n;
  NC = _NC;
  colors = (int *)malloc(sizeof(int)*2*n);
  shuf   = (int *)malloc(sizeof(int)*n);
  for(int i = 0; i<n; i++){
    shuf[i] = i;
  }
}

ColorIntegrator::~ColorIntegrator(){
  std::free(colors);
  std::free(shuf);
}

void ColorIntegrator::UniformColors(){
  for(int i = 0; i<2*n; i++){
    colors[i] = getRand(0,NC);
  }
}

// Dice colors as described in 0808.3674.
// This methods leads to matching color and anti color numbers
// but not always to valid colors flows
void ColorIntegrator::Colors(){
  for(int i = 0; i<n; i++){
    colors[2*i] = getRand(0,NC);
  }
  full_shuffle(n, shuf);
  for(int i = 0; i<n; i++){
    colors[2*i+1] = colors[2*shuf[i]];
  }
}

void ColorIntegrator::NewColors(){
  // Color Sampling a la mk
  // Goal is to also always find at least one permutation
  //    which would be preferably on the gpu since we would
  //    not generate events whose corresponding matrix element
  //    we do not evaluate once
  full_shuffle(n, shuf);
  colors[2*shuf[0]] = getRand(0,NC);
  for(int i = 0; i<n; i++){
    colors[2*shuf[i]+1] = colors[2*shuf[i-1]];
    colors[2*shuf[i]  ] = getRand(0,NC);
  }
  colors[2*shuf[0]+1] = colors[2*shuf[n-1]];
}

real_float ColorIntegrator::UniformWeight(){
  return pow(NC, 2*n);
}

real_float ColorIntegrator::Weight(){
  int sum[NC];
  memset(sum, 0, NC*sizeof(int));
  for(int i = 0; i<n; i++){
    sum[colors[2*i]] ++;
  }
  real_float w = 1.0;
  w *= pow(NC,n);
  w *= fact(n);
  for(int i = 0; i<NC; i++){
    w /= fact(sum[i]);
  }
  return w;
}

real_float ColorIntegrator::NewWeight(){
  return 1.;
}

bool ColorIntegrator::trivialcheck(){
  int sum[2*NC];
  memset(sum, 0, 2*NC*sizeof(int));
  for(int i = 0; i<n; i++){
    sum[2*colors[2*i]] ++;
    sum[2*colors[2*i+1]+1] ++;
  }
  for(int i = 0; i<NC; i++){
    if(sum[2*i] != sum[2*i+1]){
      found = false;
      return false;
    }
  }
  return true;
}

bool ColorIntegrator::findperms(std::vector<int> perm, std::vector<int> inds){
  // found full permutation
  if(perm.size() == n){
    std::reverse(perm.begin(),perm.end());
    perms.push_back(perm);
    found = true;
    return true;
  }
  
  // recursion
  for(size_t i = 0; i<inds.size(); i++){
    int ind = inds[i];
    if(colors[2*ind] == colors[2*perm.back()+1]){
      std::vector<int> linds(inds);
      std::vector<int> lperm(perm); 
      linds.erase(linds.begin() + i);
      lperm.push_back(ind);
      findperms(lperm, linds);
    }
  }
  return false;
}

bool ColorIntegrator::findperms(){
  found = false;
  perms.clear();
  if(!trivialcheck()){
    return found;
  }

  std::vector<int> perm;
  std::vector<int> inds;
  for(int i = 0; i<n-1; i++){
    inds.push_back(i);
  }
  perm.push_back(n-1);
  findperms(perm, inds);
  return found;
}


















