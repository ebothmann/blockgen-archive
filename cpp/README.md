# `cuda` Implementations:

This folder contains different color treatments to compute matrix-elements of gluon-scattering.

`/COLOR_SUMMED_F` Color-Summed matrix elements in the adjoiunt representation.

`/COLOR_SAMPLED_CF` Color-Sampled matrix elements, using the color-flow representation.

`/LEADING_COLOR` Color-Sampled matrix elements in the leading color approximation.

# Building
Move to one of the subfolders and execute:
```
mkdir build
cd build
cmake .. -DUSE_DOUBLE_PRECISION=1
make
```

Running the resulting executable without arguments will show the possible
parameters. 
