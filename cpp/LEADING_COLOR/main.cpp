#include <algorithm>
#include <array>
#include <iostream>
#include <math.h>
#include <cstdlib>
#include "config.h"
#include <chrono>
#include <memory>
#include <vector>
#include <getopt.h>

// LHAPDF
#ifdef LHAPDF_FOUND
#include "LHAPDF/LHAPDF.h"
using namespace LHAPDF;
#endif

// Remaining functionality
#include "../tools/vec4.h"
#include "../tools/rambo.h"
#include "../tools/cuts.h"
#include "../tools/tools.h"
#include "../tools/system.h"

// local functions
#include "cobg.h"

int print_short_help_and_exit(int argc, char **argv){
  std::cout << "Usage: " << argv[0]
            << " -n <nout> -e <nevt_power> -R <random_seed>"
	    << " [--sum-hels] [--me2-only]";
  exit(1);
}

int main(int argc, char **argv){

  if (argc < 5)
    print_short_help_and_exit(argc, argv);

  // compile-time constants
  constexpr int nin = 2;
  constexpr int NC = 3;
  constexpr real_float ecms = 14000;
  constexpr real_float mz = 91.188;
  constexpr real_float mz2 = mz*mz;
  constexpr real_float conversion = 0.389379*1e9;

  // user options
  int nout = 2;
  int sum_helicities = 0;
  int me2_only = 0;
  int nevt_power = 0;
  int verbose = 0;

  // parse environment variables
  std::string data_path = get_environment_variable("COBG_DATA_PATH", "../../dat");

  // parse cmdline options
  // adapted from https://www.informit.com/articles/article.aspx?p=175771&seqNum=3
  struct option longopts[] = {
    { "nout",        required_argument, NULL,            'n' },
    { "nev",         required_argument, NULL,            'e' },
    { "random-seed", required_argument, NULL,            'R' },
    { "sum-hels",    no_argument,       &sum_helicities, 1   },
    { "me2-only",    no_argument,       &me2_only,       1   },
    { "verbose",     no_argument,       &verbose,        1   },
    { 0, 0, 0, 0 }
  };
  int getopt_err = 0;
  int c;
  while ((c = getopt_long(argc, argv, ":n:e:pmv", longopts, NULL)) != -1) {
    switch (c) {
      case 'n':
	nout = atoi(optarg);
	break;
      case 'e':
	nevt_power = atoi(optarg);
	break;
      case 'R':
	srand(atoi(optarg));
	break;
      case 'p':
	sum_helicities = 1;
	break;
      case 'm':
	me2_only = 1;
	break;
      case 'v':
	verbose = 1;
	break;
      case 0:     /* getopt_long() set a variable, just keep going */
	break;
#if 0
      case 1:
	/*
	 * Use this case if getopt_long() should go through all
	 * arguments. If so, add a leading '-' character to optstring.
	 * Actual code, if any, goes here.
	 */
	break;
#endif
      case ':':   /* missing option argument */
	fprintf(stderr, "%s: option `-%c' requires an argument\n",
	    argv[0], optopt);
	getopt_err = 1;
	break;
      case '?':
      default:    /* invalid option */
	fprintf(stderr, "%s: option `-%c' is invalid: ignored\n",
	    argv[0], optopt);
	getopt_err = 1;
	break;
    }
  }
  if (getopt_err)
    print_short_help_and_exit(argc, argv);

  const int n = nin + nout;
  const int nev = pow(10, nevt_power);

  Rambo gen(nin,nout,ecms);
  cobg BG(n);

  real_float moms[4*n];
  bool valid;
  real_float x1 = 1.0;
  real_float x2 = 1.0;
  real_float dcs, psweight, alphas;

  real_float sum = 0;
  real_float sum2 = 0;
  int n_valid = 0;
  int n_samples = 0;

  // array for the permutations of momenta
  int shuf[n];
  for(int i = 0; i<n-1; i++){
    shuf[i] = i;
  }
  shuf[n-1] = (n-1)*n/2.;

  std::cout << "Set up for gg->" << n-2 << "g event generation\n";
  bool print_progress = nev >= 100;

  // variables used for the polarisation sum/sampling
  RealMom polarisation_vectors[2*n];
  Polarisation pols[n];

  //
  // START MONTE CARLO LOOP
  // 
  auto t0 = std::chrono::high_resolution_clock::now();
  while(n_valid<nev){
    if (print_progress && n_valid % (nev/100) == 0)
      std::cout << "\rGenerating... " << std::ceil(n_valid/float(nev)*100)
		<< " %" << std::flush;

    gen.generatePoint(moms, x1, x2);
    valid = cuts(n,moms, 20., 2.5, 0.4);
    n_samples++;
    if (valid){
      n_valid ++;
      psweight = gen.generateWeight(moms,x1,x2);
      shuffle(n,shuf);
      alphas = 0.118;

      dcs = 1.0;
      dcs *= n - 1; // permutation conversion as in 1002.3446

      fill_real_polarisation_vectors(polarisation_vectors, moms, n);
      BG.init_P(moms, shuf);

      // MATRIX ELEMENT
      if(sum_helicities){
	// Polarisation sum
	double me2_plus  = 0;
	double me2_minus = 0;
	for(unsigned int ii = 0; ii<(1 << (n-1)); ii++){
	  // Fill polarisation array
	  for(unsigned int j = 0; j<n; j++){
	    pols[j] = static_cast<Polarisation>((ii & (1 << j)) >> j);
	  }
	  BG.getME(moms,shuf,polarisation_vectors,pols);
	  me2_plus  += pow(BG.J[BG.NN-1]*BG.J[BG.NN  ],2);
	  me2_minus += pow(BG.J[BG.NN-1]*BG.J[BG.NN+1],2);
	}
	dcs *= me2_plus + me2_minus;
	dcs *= pow(0.5,n);
      }
      else{
	select_random_polarisations(polarisation_vectors, n);
	std::fill(pols, pols + n, Polarisation::xlin);
	BG.getME(moms,shuf,polarisation_vectors,pols);
	dcs *= pow(BG.J[BG.NN-1]*BG.J[BG.NN + static_cast<int>(pols[n-1])],2);
      }

      dcs /= x1 * x2 * 2 * pow(ecms, 2.0); // flux factor
      dcs *= psweight; // phase-space weight
      dcs *= conversion; // conversion to pb
      dcs *= pow(sqrt(4.*M_PI*alphas),2*n-4); // ME factor
      dcs *= pow(NC, n-2) * (NC*NC-1); // color factor
      dcs /= 64.; // color average
      sum += dcs;
      sum2 += pow(dcs,2);
    }
  }
  auto t1 = std::chrono::high_resolution_clock::now();
  auto t_micros_tot = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count();

  if (print_progress)
      std::cout << "\r";
  std::cout << "Generating... done\n";
  
  real_float AA = sum / n_samples;
  real_float sAA = std::sqrt(( sum2 / n_samples  - AA*AA) / n_samples);
  real_float ref_avg[12-3] = {2.32421e+8, 1.4353e+7, 2.84780e+6, 6.356e+5, 1.608e+5, 4.38e+4, 1.193e+4, 3.550e+4, 9.64e+3};
  real_float ref_std[12-3] = {0.00047e+8, 0.0011e+7, 0.00096e+6, 0.012e+5, 0.011e+4, 0.11e+4, 0.024e+4, 0.020e+4, 0.74e+3};
  
  // also calculate "Rivet-style" deviation in terms of number of standard
  // deviations (taking into account the standard deviation of both our result
  // and the reference result)
  real_float deviation = (AA - ref_avg[n-4]) / sqrt(pow(ref_std[n-4], 2) + pow(sAA, 2));
  real_float deviation_err = sAA / ref_std[n-4];
  
  std::cout << "|A|^2               = ";
  std::cout << AA << " +/- " << sAA;
  std::cout << '\n';
  std::cout << "Sampling efficiency = " << n_valid * 1./n_samples << '\n';
  std::cout << "Event loop duration = " << t_micros_tot*1e-6 << " s" << std::endl;
  
  return 1;
}
